//
//  MainViewController.swift
//  DoneCoNote
//
//  Created by Dima on 3/28/19.
//Copyright © 2019 Volpis. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    private var viewModel: MainViewModel!
    
    
    // MARK: - life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel = MainViewModel(view: self)
        setupUI()
    }
    
    
    // MARK: - private
    
    private func setupUI() {
        
    }
}


//MARK: - MainView protocol

extension MainViewController: MainView {
    
}
