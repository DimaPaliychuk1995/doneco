//
//  MainViewModel.swift
//  DoneCoNote
//
//  Created by Dima on 3/28/19.
//Copyright © 2019 Volpis. All rights reserved.
//

import Foundation

protocol MainView: AnyObject {
    
}

class MainViewModel: NSObject {
    
    private weak var view: MainView?
    
    
    //MARK: - life cycle
    
    init(view: MainView) {
        self.view = view
        super.init()
        
        setup()
    }
    
    
    // MARK: - public
    
    
    
    //MARK: - private
    
    private func setup() {
       
    }
}
